+++
title = "About"
date = "2018-07-18"
author = "Radek"
+++

<h2>> Hi there<span class="logo__cursor" style="width: 14px; border-radius: 1px; height: 1.525rem;"></span></h2>

My name is Radek and I'm the author of this theme. I made it to help you present your ideas easier.

We all know how hard is to start something on the web, especially these days. You need to prepare a bunch of stuff, configure them and when that’s done — create the content.

This theme is pretty basic and covers all of the essentials. All you have to do is start typing!

The theme includes:

- **dark/light mode**, depending on your preferences (dark is default, but you can change it)
- great reading experience thanks to [**Inter UI font**](https://rsms.me/inter/), made by [Rasmus Andersson](https://rsms.me/about/)
- nice code highlighting thanks to [**PrismJS**](https://prismjs.com)
- an easy way to modify the theme (**Webpack, NodeJS, PostCSS etc.**)
- fully responsive

So, there you have it... enjoy!
